program http_test;
{$librarypath '..'} 
uses http_client;

var
    url: pChar = 'N:https://jsonbase.com/fujinet/test';
    headers: pChar = 'Content-Type: application/json'#0'Cache-Control: no-cache'#0#0;
    request: string = '{"test":"OK"}';
    response: string ;
    
begin
	HTTP_headers := @headers;
	HTTP_Post(@url, @response[1], @request[1], Length(request));
	
	if HTTP_error <> 1 then writeln(HTTP_error);
	
	SetLength(response, HTTP_respSize);
	Writeln(response);
end.
