const Router = require('koa-router');
const fs = require('fs');
const request = require('request');
const gm = require('gm').subClass({ imageMagick: true });

const router = new Router({
	prefix: '/gfx'
});

const default1bpp = {
	threshold: 66,
	size: {x:320, y:192},
	background: "white",
	bitdepth: 1
}

function gmAsync(body) {
	const settings = Object.assign({}, default1bpp, body);
	const image = (body.url)?request(body.url):body.path;
	console.log(`Processing: ${(body.url)?body.url:body.path}`);

	return new Promise(function(resolve,reject){
		gm(image).
		resize(settings.size.x,settings.size.y).
		colorspace('gray').
		background(settings.background).
		gravity('Center').
		extent(settings.size.x,settings.size.y).
		threshold(`${settings.threshold}%`).
		bitdepth(settings.bitdepth).
		toBuffer('GRAY',function (err, buffer) {
		    if (err) {
				reject(err);
			} else {
				console.log('Done.');
				resolve(buffer);
			}
		})
	})
}

router.post('/convert', async (ctx, next) => {
	if ( !(ctx.request.body.path || ctx.request.body.url) || !ctx.request.body.mode ) {
		ctx.response.status = 400;
		ctx.body = {
			status: 'error',
			message: 'Missing params'
	    	}
		await next();
	} else {
		ctx.response.status = 201;
		ctx.type = 'application/octet-stream';
		ctx.body = await gmAsync(ctx.request.body);
		next();
	}
});

module.exports = router;
