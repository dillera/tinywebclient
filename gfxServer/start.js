const Koa = require('koa');
const koaBody = require('koa-body');

const app = new Koa();

// Set up body parsing middleware
app.use(koaBody());

// Require the Router we defined 
let magick = require('./magick.js');

// Use the Router on the sub route /gfx
app.use(magick.routes());
console.log('server started');
app.listen(32123);

